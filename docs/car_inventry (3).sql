-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2021 at 07:40 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beta`
--

-- --------------------------------------------------------

--
-- Table structure for table `car_inventry`
--

CREATE TABLE `car_inventry` (
  `PART NUMBER` int(11) NOT NULL,
  `ITEM NAME` varchar(50) NOT NULL,
  `QTY IN STOCK` int(11) NOT NULL,
  `QTY REQUIRED` int(11) NOT NULL,
  `QTY ORDERD` int(11) NOT NULL,
  `ORDERD ID` varchar(20) NOT NULL,
  `DATE ORDER PLACED` varchar(10) NOT NULL,
  `STOCK LOCATION` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `car_inventry`
--

INSERT INTO `car_inventry` (`PART NUMBER`, `ITEM NAME`, `QTY IN STOCK`, `QTY REQUIRED`, `QTY ORDERD`, `ORDERD ID`, `DATE ORDER PLACED`, `STOCK LOCATION`) VALUES
(1, 'FORD', 11, 50, 39, 'FDE', '08/10/2021', 'KORADI NAGPUR,441111'),
(2, 'SUZUKI', 5, 12, 7, 'SUZU', '25/09/2021', 'KADBHICHOWK NAGPUR-440014'),
(3, 'HUNDAI I20', 25, 50, 25, 'I20', '12/08/2021', 'SADAR NAGPUR-440004');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car_inventry`
--
ALTER TABLE `car_inventry`
  ADD PRIMARY KEY (`PART NUMBER`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car_inventry`
--
ALTER TABLE `car_inventry`
  MODIFY `PART NUMBER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
